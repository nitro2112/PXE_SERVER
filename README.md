To install PXE server you must:

1.Download ubuntu-16.04-server-amd64.iso from 
	http://old-releases.ubuntu.com/releases/16.04.1/
	
	and copy it to this folder.

2.Run install.sh script.

NOTE: 
	You can boot via tftpboot your bootable filesystem. To do this you must
		edit "/var/lib/tftpboot/pxelinux.cfg/default" file like in this link:
			https://pubs.vmware.com/vsphere-4-esx-vcenter/index.jsp?topic=/com.vmware.vsphere.installclassic.doc_41/install/boot_esx_install/c_about_pxe_config_files.html 
	You can manage who can access to boot filesystem via PXE server. To do
		this you must edit "/etc/dhcp/dhcpd.conf" file as follow:
			I. Change line :"range 192.168.1.21 192.168.1.50;" 
				     to:"range 192.168.1.21 192.168.1.21;"
			II. Add these lines at bottom:
				"host foo { 
   				  hardware ethernet <mac address>; 
    				  fixed-address <ip address>;
				 }"
