#!/bin/bash

#Install, configure and startup DHCP server

sudo apt-get install isc-dhcp-server

sudo cp isc-dhcp-server /etc/default/
sudo cp dhcpd.conf /etc/dhcp/

sudo ifconfig enp0s25 192.168.1.105
sudo systemctl restart isc-dhcp-server


#install and configure tftp

sudo apt-get install apache2 tftpd-hpa inetutils-inetd

sudo cp tftpd-hpa /etc/default/
sudo cp inetd.conf /etc/

sudo kill $(sudo lsof -t -i:69)
sudo systemctl restart tftpd-hpa

#mount ubuntu ISO and copy files into tftpboot

sudo mount -o loop ubuntu-16.04-server-amd64.iso /mnt/
sudo cp -fr /mnt/install/netboot/* /var/lib/tftpboot/
sudo mkdir /var/www/html/ubuntu
sudo cp -fr /mnt/* /var/www/html/ubuntu/

sudo cp default /var/lib/tftpboot/pxelinux.cfg/
